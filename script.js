// HTML elements
const elemCardButton = document.querySelector(".card");
const elemEscapeButton = document.querySelector(".escape");
const elemWrapper = document.querySelector(".wrapper");
const elemSubmitButton = document.querySelector("#submitButton");
const elemCard1 = document.querySelector("#card1");
const elemCard2 = document.querySelector(".card2");
const elemInputText = document.querySelector("#inputText");
const elemDescriptionTextArea = document.querySelector("#descriptionTextArea");
const elemMainWrapper = document.querySelector("mainWrapper");
let editId = 0;

const elemBody = document.querySelector("body");
// this unhides my form and clears the inputs when button is clicked
elemCardButton.addEventListener("click", function (event) {
  event.preventDefault();
  editId = 0;
  elemWrapper.style.display = "block";
  elemWrapper.style.position = "absolute";
  elemWrapper.style.zIndex = "1";
  elemInputText.value = "";
  elemDescriptionTextArea.value = "";
  elemBody.style.backgroundColor = "#7152c5fd";
  elemBody.style.transition = "all 0.35s ease";
});

// this hides the form when the x is clicked
elemEscapeButton.addEventListener("click", function (event) {
  event.preventDefault();
  elemWrapper.style.display = "none";
  elemBody.style.backgroundColor = "#9772fb";
});

// this hides the form and creates a new card with info entered from the form
const myCardsArr = [];

function deleteArrFunc() {
  const eachCard = document.querySelectorAll(".noteWrapper");
  eachCard.forEach((selectedCard) => {
    selectedCard.remove();
  });
}

elemSubmitButton.addEventListener("click", function (event) {
  event.preventDefault();
  // this removes items of array from dom
  elemBody.style.backgroundColor = "#9772fb";
  deleteArrFunc();
  myCardsArr.push({
    id: Date.now(),
    noteTitle: elemInputText.value,
    description: elemDescriptionTextArea.value,
  });

  addToDom();
});

let trashIcon = document.createElement("button");
trashIcon.classList.add = "trashIcon";
let pencilIcon = document.createElement("button");

function addToDom() {
  for (let i = 0; i < myCardsArr.length; i++) {
    const clickedCard = myCardsArr[i];
    elemWrapper.style.display = "none";
    elemCard1.style.display = "flex";
    let noteTitle = document.createElement("p");
    let noteDescription = document.createElement("p");
    // let noteId = document.createElement("div");
    let footer = document.createElement("footer");
    let ellipsisButton = document.createElement("div");
    let noteWrapper = document.createElement("div");
    ellipsisButton.classList.add("ellipsisClass", "ellipsisClass2");
    ellipsisButton.innerText = "...";
    noteWrapper.classList.add("noteWrapper");
    noteTitle.classList.add("noteTitle");
    noteDescription.classList.add("noteDescription");
    // noteId.id.add("noteId");
    let toggledText = document.createElement("div");
    toggledText.classList.add("toggledText");
    // toggledText.setAttribute("id", myCardsArr[i].id);
    footer.classList.add("footer");
    noteTitle.innerHTML = clickedCard.noteTitle;
    noteDescription.innerHTML = clickedCard.description;
    // noteId.textContent = clickedCard.id;
    footer.innerHTML = new Date().toLocaleDateString();
    footer.style.color = "#6e3cbc";
    footer.style.paddingTop = "10px";
    footer.style.fontSize = "16px";
    // console.log({ myCardsArr });
    footer.appendChild(ellipsisButton);
    noteWrapper.appendChild(noteTitle);
    noteWrapper.appendChild(noteDescription);
    // noteWrapper.appendChild(noteId);
    noteWrapper.appendChild(footer);
    elemCard1.appendChild(noteWrapper);

    // Icon elements created
    // trashIcon.setAttribute("id", clickedCard.id);

    console.log("trash", trashIcon);
    trashIcon.id = clickedCard.id;
    console.log("clickedCard", clickedCard.id);
    trashIcon.innerHTML =
      'Trash <i class="fa fa-trash"" aria-hidden="true"></i>';
    pencilIcon.innerHTML = 'Edit <i class="fa fa-pencil"" title="Edit"></i>';
    ellipsisButton.addEventListener("click", function (event) {
      event.preventDefault();
      toggledText.appendChild(trashIcon);
      toggledText.appendChild(pencilIcon);
      ellipsisButton.appendChild(toggledText);
      ellipsisButton.classList.toggle("ellipsisClass");
      if (toggledText.style.display == "block")
        toggledText.style.display = "none";
      else toggledText.style.display = "block";
    });
  }
}
// -------------------------------------------------
trashIcon.addEventListener("click", function (event) {
  // event.preventDefault();
  let foundCard = null;
  const item = event.target;
  console.log(item);
  for (i = 0; i < myCardsArr.length; i++) {
    if (item.id == myCardsArr[i].id) {
      foundCard = myCardsArr[i];
      break;
    } else {
      console.log("item", item);
      console.log("arr", myCardsArr);
      console.log("foundCard", foundCard);
      console.log("error");
    }
  }
  myCardsArr.splice(foundCard, 1);
  deleteArrFunc();
  addToDom();
});

pencilIcon.addEventListener("click", function (event) {
  event.preventDefault();
  const edit = event.target.id;
  // editId = edit.id;
  let currentItem = {};
  for (let i = 0; i < myCardsArr.length; i++) {
    // pencilIcon.setAttribute("id", myCardsArr[i].id);
    console.log("edit Var", edit);
    if (myCardsArr[i].id == edit) {
      elemWrapper.style.display = "block";
      currentItem = myCardsArr[i];
      console.log(currentItem);
      console.log(currentItem.noteTitle);
      elemInputText.innerText = currentItem.noteTitle;
      elemDescriptionTextArea.value = currentItem.noteDescription;
    }
  }
  // elemInputText.value = currentItem.noteTitle;
});
// const trashItem = document.querySelector(myCardsArr[i].id);
// console.log("trashItem:", trashItem);
// if (item === trashItem)
// const note =
//   item.parentElement.parentElement.parentElement.parentElement
//     .parentElement;
// trashItem.remove();
// this displays the edit and trash button when the ellipses is clicked

//this is the trash button. It deletes the entered note
// this is the edit button listener

// const cardId1 =
//   e.target.parentElement.parentElement.parentElement.parentElement.id;
// console.log(cardId1);

// elemInputText.value = elemInputText.value;
// elemDescriptionTextArea.value = "";
